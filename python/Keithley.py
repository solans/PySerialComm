#!/usr/bin/env python
#############################################
# Keithley 2400 
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# February 2016
#############################################
'''
- In voltage source mode, READ? MEAS? and MEAS:CURR? return the 
correct voltage AND current, while MEAS:VOLT? only returns
the correct voltage (with current at +9.91e37)
- In current source mode, READ? MEAS? and MEAS:CURR? return the
correct current only (!) (with voltage at +9.91e37)
MEAS:VOLT? returns the correct voltage only (with current at +9.91e37)
- ref Manual page 431 (http://research.physics.illinois.edu/bezryadin/labprotocol/Keithley2400Manual.pdf)
'''

from time import sleep
from SerialCom import SerialCom

class Keithley:
    sc = None
    def __init__(self,portname,baudrate=19200,timeout=0.5):
        self.sc = SerialCom(portname,baudrate=baudrate,timeout=timeout)
        pass
    def setVerbose(self,enable):
        self.sc.setVerbose(True)
        pass
    def beep(self):
        self.sc.write(":BEEP")
        pass
    def setCurrentLimit(self,fvalAmps):
        self.sc.write(":SENSE:CURR:PROT %.4f"%fvalAmps)
        pass
    def setVoltageHighPrecision(self):
        self.sc.write(":SENSE:VOLT:NPLCYCLES 10")
        pass
    def setCurrentHighPrecision(self):
        self.sc.write(":SENSE:CURR:NPLCYCLES 10")
        pass
    def setVoltageRange(self,fvalVolt):
        self.sc.write(":SENSE:VOLT:RANG %.4f"%fvalVolt)
        pass
    def setCurrentRange(self,fvalCurr):
        self.sc.write(":SENSE:CURR:RANG %.4f"%fvalCurr)
        pass
    def setVoltageLimit(self,fvalVolts):
        self.sc.write(":SENSE:VOLT:PROT %.4f"%fvalVolts)
        pass
    def setCurrent(self,fsetValAmps):
        self.sc.write(":SOUR:CURR %.4f"%fsetValAmps)
        pass
    def setVoltage(self,fsetValVolts):
        if abs(fsetValVolts)>=20:
            self.sc.write(":SOUR:VOLT:RANGE 1000")
            pass
        else:
            self.sc.write(":SOUR:VOLT:RANGE 20")
            pass
        pass
        self.sc.write(":SOUR:VOLT %.4f"%fsetValVolts)
    def enableOutput(self,bEnable):
        cmd="ON" if bEnable==True else "OFF"
        #print "Switching output "+cmd
        self.sc.write(":OUTPUT %s" % cmd)
        pass
    def getSourceMode(self):
        return self.sc.writeAndRead(":SOUR:FUNC?")
    def close(self):
        self.sc.close()
        pass
    def getVoltage(self):
        v = self.sc.writeAndRead(":READ?")
        #v = self.sc.writeAndRead(":MEAS:VOLT?")
        if not "," in v: return -999.
        return float(v.split(",")[0])
    def getCurrent(self):
        v = self.sc.writeAndRead(":READ?")
        #v = self.sc.writeAndRead(":MEAS:CURR?")
        if not "," in v: return -999.
        return float(v.split(",")[1])
    def getSetVoltage(self):
        v = self.sc.writeAndRead(":SOUR:VOLT?")
        if not v: return -999.
        return float(v)
    def getSetCurrent(self):
        v = self.sc.writeAndRead(":SOUR:CURR?")
        if not v: return -999.
        return float(v)
    def setVoltSrc(self):
        self.setSourceVoltage()
        pass
    def setCurrSrc(self):
        self.setSourceCurrent()
        pass
    def setSourceVoltage(self):
        self.sc.write(":SOUR:FUNC VOLT")
        self.sc.write(":SENS:FUNC 'CURR'")
        pass
    def setSourceCurrent(self):
        self.sc.write(":SOUR:FUNC CURR")
        self.sc.write(":SENS:FUNC 'VOLT'")
        pass
    def getCurrentLimit(self):
        v = self.sc.write(":SENSE:CURR:PROT?")
        if not v: return -999.
        return float(v)
    def userCmd(self,cmd):
        print ("userCmd: %s" % cmd)
        return self.sc.writeAndRead(cmd)
    def reset(self):
        self.sc.write("*RST")
        pass
    def isEnabled(self):
        v=self.sc.writeAndRead(":OUTPut?")
        return True if v == "1" else False
    def getModel(self):
        resp = self.sc.writeAndRead("*IDN?")
        return resp
    def rampVoltage(self,fsetNewValVolts,iSteps,isetDelay):
        safe = True
        V = self.getVoltage()
        V = round(V,4)
        if abs(fsetNewValVolts-V)<1: 
            self.setVoltage(fsetNewValVolts)
            return
        s = 1
        iSteps = int(iSteps+1)
        Vstep = float((fsetNewValVolts-V)/(iSteps-1))
        while s < (iSteps):
            v = self.getVoltage()
            if v == -999:
                print ("Error in voltage being set")
                safe = False 
                pass
            if safe == False: break
            self.sc.write(":SOUR:VOLT %f"%(Vstep*s+V))
            sleep(isetDelay)
            print ("Ramping Voltage: %.4f"%(Vstep*s+V))
            s += 1
            pass
        pass
    pass
