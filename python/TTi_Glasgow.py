#!/usr/bin/env python
#############################################
# TTI power supply control
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# February 2016
#############################################

from SerialCom import SerialCom
import socket
    
class TTi_Glasgow:
    sc = None
    def __init__(self,portname):
        self.sc = SerialCom(portname,baudrate = 9600)
        self.sock_timeout_secs = 4
        self.portname = portname
        self.port = 9221

    def send(self,cmd):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(self.sock_timeout_secs)
            s.connect((self.portname, self.port))
            s.sendall(bytes(cmd,'ascii'))

    def send_receive_string(self,cmd):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(self.sock_timeout_secs)
            s.connect((self.portname, self.port))
            s.sendall(bytes(cmd,'ascii'))
            #self.mysocket.sendall(bytes(cmd,'ascii'))
            #data = self.recv_end(self.mysocket)
            data = s.recv(1024)
        return data.decode('ascii')

    def send_receive_float(self,cmd):
        r = self.send_receive_string(cmd)
        r = r.rstrip("\r\nVA")
        l = r.rsplit()
        if len(1) > 0:
           return float(l[-1])
        return 0.0

    def ListenDevice(self):
        self.sc.write("02H")
	self.sc.write("12H f")

    def setVoltageLimit(self,iOutput,fValue):
        self.sc.write("OVP%i %f"%(iOutput,fValue))

    def endCommand(self):
        self.sc.write("0DH 0AH")

    def endCommandGroup(self):
        self.sc.write("3BH")

    def setCurrentLimit(self,iOutput,fValue):
        self.sc.write("OCP%i %f"%(iOutput,fValue))

    def setVoltage(self,iOutput,fValue):
        self.sc.write("V%i %f"%(iOutput,fValue))

    def getVoltage(self,iOutput):
        return self.sc.writeAndRead("V%iO?"%iOutput)

    def getCurrent(self,iOutput):
        #cmd = ("I%iO?"%iOutput)
        #V = self.send_receive_float(cmd)
        return self.sc.writeAndRead("I%iO?"%iOutput)

    #def getCurrent(self,iOutput):
    #    return self.sc.writeAndRead("I%iO? 0AH"%iOutput)

    def enableOutput(self,iOutput,bValue):
        if bValue == True:
            self.sc.write("OP%i 1"%iOutput)
        elif bValue == False:
            self.sc.write("OP%i 0"%iOutput)

    def close(self):
        self.sc.close()
