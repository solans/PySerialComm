#!/usr/bin/env python
import os
import sys

class PSUConfig:
    def __init__(self,path):
        self.psus={}
        fr=open(path)
        for line in fr.readlines():
            line=line.strip()
            chk=line.split()
            if len(chk)==0: continue
            if chk[0].startswith("#"): continue
            if "addPSU" in chk[0]:
                psu={}
                psu["Name"]=chk[1]
                psu["Port"]=chk[2]
                psu["Type"]=chk[3]
                psu["Chan"]=int(chk[4])
                psu["Vmax"]=float(chk[5])
                psu["Imax"]=float(chk[6])
                self.psus[chk[1]]=psu
                pass
            pass
        pass
    def getPsus(self):
        return self.psus
    pass

