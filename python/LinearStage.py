#!/usr/bin/env python
#############################################
# LinearStage SR1
#
# Carlos.Solans@cern.ch
# June 2016
#############################################
# Berger Larhr ICIA IDS-MIO / IL1R RS485
# Port 9600 bps 7-E-1 profibus protocol
# Messages are little endian ASCII chars
# Byte arrays need cast to hex strings
# Control message from controller to stage
# Byte 1:   [7] toggle bit
#           [2] read(0) or write(1)
# byte 2:   [7,0] sub-index
# byte 3-4: [15,0] index
# byte 5-8: [31,0] command
# Reply message from stage to controller
# Byte 1:   [7] toggle bit
#           [6] error bit
# byte 2:   [7,0] control data
# byte 3-4: [15,0] status word
# byte 5-8: [31,0] data

from SerialCom import SerialCom
import time

class LinearStage:
    def __init__(self,portname):
        self.mm2inc = 2000
        self.inc2mm = 0.0005
        self.maxpos = 1000
        self.verbose = False
        self.sc = SerialCom(portname,
                            timeout=1,
                            bytesize=7,
                            parity='E',
                            baudrate=9600,
                            stopbits=1)
        #init comm
        reply=self.sc.writeAndRead('#1')
        if reply!='#1': print "LinearStage master communication failed"
        #toggle bit
        self.rd = True
        pass

    def setVerbose(self,verbose):
        self.verbose=verbose
        self.sc.setVerbose(verbose)
        
    def getTb(self):
        self.rd=not self.rd
        return self.rd
    
    @staticmethod
    def encodeControl(rd,io,index,subindex,command):
        byte_array=[0,]*8
        byte_array[0] = (0x80 if rd else 0x00) | (0x04 if io else 0x00)
        byte_array[1] = (subindex&0xFF)
        byte_array[2] = (index>>7)&0xFF
        byte_array[3] = (index>>0)&0xFF
        byte_array[4] = (command>>23)&0xFF
        byte_array[5] = (command>>15)&0xFF
        byte_array[6] = (command>> 7)&0xFF
        byte_array[7] = (command>> 0)&0xFF
        return ''.join('{:02X}'.format(x) for x in byte_array)
        
    @staticmethod
    def decodeControl(hex_string):
        byte_array=bytearray.fromhex(hex_string)
        data={'rd':0,'io':0,'index':0,'subindex':0,'command':0}
        data['rd']       = (byte_array[0]&0x80)>>7
        data['io']       = (byte_array[0]&0x04)>>2
        data['subindex'] = (byte_array[1]&0xFF)
        data['index']    = (byte_array[2]<<8 | byte_array[3]<<0)
        data['command'] |= (byte_array[4]<<24)
        data['command'] |= (byte_array[5]<<16)
        data['command'] |= (byte_array[6]<<8)
        data['command'] |= (byte_array[7]<<0) 
        return data
    
    @staticmethod
    def encodeReply(rd,control,status,data):
        byte_array=[0,]*8
        byte_array[0] = (0x80 if rd else 0x00)
        byte_array[1] = (control&0xFF)
        byte_array[2] = (satus>>8)&0xFF
        byte_array[3] = (satus>>0)&0xFF
        byte_array[4] = (data>>24)&0xFF
        byte_array[5] = (data>>16)&0xFF
        byte_array[6] = (data>> 8)&0xFF
        byte_array[7] = (data>> 0)&0xFF
        return ''.join('{:02X}'.format(x) for x in byte_array)

    @staticmethod
    def decodeReply(hex_string):
        byte_array=bytearray.fromhex(hex_string)
        data={'rd':0,'control':0,'status':0,'data':0}
        data['rd']      = (byte_array[0]&0x80)>>7
        data['control'] = (byte_array[1]&0xFF)
        data['status']  = (byte_array[2]<<8) | (byte_array[3]<<0)
        data['data']   |= (byte_array[4]<<23)
        data['data']   |= (byte_array[5]<<16)
        data['data']   |= (byte_array[6]<<8)
        data['data']   |= (byte_array[7]<<0) 
        return data

    def setPower(self,bState):	
        status=False
        for trial in xrange(5):
            if self.verbose: print "Set power %i (%i)" % (bState,trial)
            ans=self.sc.writeAndRead(self.encodeControl(self.getTb(),True,28,1,(2 if bState else 1)))
            print self.decodeReply(ans)
            status = self.getStatus();
            if status: break
            time.sleep(1)
            pass
        if self.verbose: print "Power status: %i" % status
        pass
    
    def setHomePosition(self,fPosition=1):
        #Set home velocity in
        ans=self.sc.writeAndRead(self.encodeControl(self.getTb(),True,40,4,400))
        if self.verbose: print self.decodeReply(ans)
        #Set home velocity out
        ans=self.sc.writeAndRead(self.encodeControl(self.getTb(),True,40,5,400))
        if self.verbose: print self.decodeReply(ans)
        #Set zero position
        ans=self.sc.writeAndRead(self.encodeControl(self.getTb(),True,40,7,fPosition*self.mm2inc))
        if self.verbose: print self.decodeReply(ans)
        #Start movement
        ans=self.sc.writeAndRead(self.encodeControl(self.getTb(),True,40,1,2))
        if self.verbose: print self.decodeReply(ans)
        #wait for head to reach
        print "Wait until we reach home"
        while self.isMoving(): time.sleep(1)
        pass

    def setPosition(self,fPosition):
        if fPosition >= self.maxpos:
            print "Set position %i >= %i" % (fPosition,self.maxpos)
            return
        self.sc.writeAndRead(self.encodeControl(self.getTb(),True,35,1,fPosition*self.mm2inc))
        pass
    
    def getPosition(self):
        bArray=self.sc.writeAndRead(self.encodeControl(self.getTb(),False,35,1,0))
        reply=self.decodeReply(bArray)
        if self.verbose: print reply
        return reply['data']*self.inctomm
    
    def setSpeed(self,iSpeed):
        self.sc.writeAndRead(self.encodeControl(self.getTb(),True,35,5,iSpeed))
        pass

    def getSpeed(self):
        bArray=self.sc.writeAndRead(self.encodeControl(self.getTb(),False,35,5,0))
        reply=self.decodeReply(bArray)
        if self.verbose: print reply
        return reply['data']

    def isMoving(self):
        bArray=self.sc.writeAndRead(self.encodeControl(self.getTb(),False,31,9,0))
        reply=self.decodeReply(bArray)
        if self.verbose: print reply
        return reply['data']

    def getBrake(self):
        bArray=self.sc.writeAndRead(self.encodeControl(self.getTb(),False,0x21,0x8,0))
        reply=self.decodeReply(bArray)
        if self.verbose: print reply
        return reply['data']
    
    def getStatus(self):
        bArray=self.sc.writeAndRead(self.encodeControl(self.getTb(),False,28,2,2))
        reply=self.decodeReply(bArray)
        if self.verbose: print reply
        return True if reply['data'] & 0x6 else False     

    def getMonitoring(self):
        bArray=self.sc.writeAndRead(self.encodeControl(self.getTb(),False,0x1C,0x12,0))
        reply=self.decodeReply(bArray)
        if self.verbose: print reply
        return reply['data']

    def getStopFault(self):
        bArray=self.sc.writeAndRead(self.encodeControl(self.getTb(),False,0x20,0x07,0))
        reply=self.decodeReply(bArray)
        if self.verbose: print reply
        return reply['data']

    def getActionStatus(self):
        bArray=self.sc.writeAndRead(self.encodeControl(self.getTb(),False,0x1C,0x13,0))
        reply=self.decodeReply(bArray)
        if self.verbose: print reply
        return reply['data']
    
    def resetFault(self):
        bArray=self.sc.writeAndRead(self.encodeControl(self.getTb(),True,0x1C,1,8))
        reply=self.decodeReply(bArray)
        if self.verbose: print reply
        pass

    pass

if __name__=='__main__':
    s="0404002800030DA0"
    print "decode %s" %s, LinearStage.decodeReply(s)
    s="8004E02400000000"
    print "decode %s" %s, LinearStage.decodeReply(s)
    s="0009001F00000000"
    print "decode %s" %s, LinearStage.decodeReply(s)
    s="8004E02400000000"
    
