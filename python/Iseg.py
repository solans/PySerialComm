#!/usr/bin/env python
# -*- coding: utf-8 -*-
#############################################
# Iseg SHQ-224M communication class 
# http://iseg-hv.com/files/media/SHQx2x_11_eng.pdf
#
# Dingane.Reward.Hlaluku@cern.ch
# Carlos.Solans@cern.ch
# June 2016
#############################################

from SerialCom import SerialCom

class Iseg:
    #Open
    def __init__(self, portname='/dev/ttyUSB0', baudrate=9600):
        self.sc = SerialCom(portname,baudrate=baudrate)
        pass

    #Close
    def close(self):
        self.sc.close()
        
    # Read module identifier
    def getModuleID(self):
        cmd = "#"
        # Write and read to Iseg in order to receive module ID
        moduleID = self.sc.writeAndRead(cmd)
        readModID = self.sc.read()
        return readModID;     
        
    # Read break time
    def getBreakTime(self):
        cmd = 'W'
        breakTime = self.sc.writeAndRead(cmd)
        readBreakTime = self.sc.read()
        return int(readBreakTime);
        
    # Write break time
    def setBreakTime(self,myBreakTime):
        if myBreakTime<2 or myBreakTime>255: return False
        cmd = 'W=%3i'%myBreakTime
        setTime = self.sc.writeAndRead(cmd)
        readTime = self.sc.read()
        return True;
        
    # Read actual voltage
    def getActualVoltage(self,channel):
        cmd = "U%i"%channel
        v = self.sc.writeAndRead(cmd)
        v1 = self.sc.read()
        vf=float(v1[:-3])*10**float(v1[-3:])
        return vf;
        
    # Read actual current
    def getActualCurrent(self,channel):
        cmd = "I%i"%channel
        writeI = self.sc.writeAndRead(cmd)
        readI = self.sc.read()
        actualI = float(readI[:-3])*10**float(readI[-3:])
        return actualI;
        
    # Read voltage limit for either channel
    def getVoltageLimit(self,channel):
        cmd = 'M%i'%channel
        voltLim = self.sc.writeAndRead(cmd)
        readVoltLim = self.sc.read()
        return int(readVoltLim);
        
    # Read current limit for either channel
    def getCurrentLimit(self,channel):
        cmd = 'N%i'%channel
        currentLim = self.sc.writeAndRead(cmd)
        readCurrenttLim = self.sc.read()
        return int(readCurrenttLim);
        
    # Read set voltage
    def getSetVoltage(self,channel):
        cmd = 'D%i'%channel
        volt = self.sc.writeAndRead(cmd)
        readV = self.sc.read()
        readVf = float(readV[:-3])*10**float(readV[-3:])
        return readVf;
        
    # Write set voltage
    def setVoltage(self,channel,fvalVolts):
        if fvalVolts > 4000: return False
        cmd = "D%i=%4.2f"%(channel,fvalVolts)
        writeVol = self.sc.writeAndRead(cmd)
        readVol = self.sc.read()
        return True;
        
    # Read ramp speed 
    def getRampSpeed(self,channel):
        cmd = 'V%i'%channel
        rSpeed = self.sc.writeAndRead(cmd)
        readSpeed = self.sc.read()
        return int(readSpeed);
        
    # Write ramp speed
    def setRampSpeed(self,channel,rampSpeed):
        if rampSpeed < 2 or rampSpeed > 255:
            return False
        else:
            cmd = 'V%i=%3i'%(channel,rampSpeed)
            writeSpeed = self.sc.writeAndRead(cmd)
            fetchSpeed = self.sc.read()
            return True;
        
    # Start voltage change, Enable output
    def enableOutput(self,channel):
        cmd = "G%i"%channel
        ret = self.sc.writeAndRead(cmd)
        status = self.sc.read()
        trmStat = status.split("=")[1]                
        if trmStat =='ON':
            print'Output voltage according to set voltage'
        elif trmStat == 'OFF':
            print'Channel front panel switch off, abort...'
            return False;
        elif trmStat == 'MAN':
            print'Channel is on, set to manual mode'
        elif trmStat == 'ERR':
            print'Max Voltage/Current was exceeded, abort..'
            return False;
        elif trmStat == 'INH':
            print'Inhibit signal is active!'
        elif trmStat == 'QUA':
            print'Quality of output voltage not given at present !'
            return False;
        elif trmStat == 'L2H':
            print'Output voltage increasing...'
        elif trmStat == 'H2L':
            print'Output voltage decreasing...'
        elif trmStat == 'LAS':
            print'Look at Status (only after G-command)'
        elif trmStat == 'TRP':
            print'Current trip was active'
        return True;
        
    # Write current trip        
    def setCurrentTrip(self,channel,fvalAmps):        
        if fvalAmps > 1 : 
            cmd ="L%i=%5i" % (channel,int(fvalAmps)) #amps
        elif fvalAmps < 0.001:
            cmd = "LS%i=%5i"%(channel,int(fvalAmps*1E6)) #micro-amps
        else:
            cmd = "LB%i=%5i"%(channel,int(fvalAmps*1E3)) #milli-amps
        writeTrip = self.sc.writeAndRead(cmd)
        readTrip = self.sc.read()
        return int(readTrip)
        
    # Read Current trip
    def getCurrentTrip(self, channel,rangeAmp=1):
        cmd = "L%i" % channel
        if rangeAmp==1:
            cmd="LB%i"%channel
        elif rangeAmp==0:
            cmd="LS%i" % channel
        v = self.sc.writeAndRead(cmd)
        v1 = self.sc.read()
        vf = v.split("=")[1] 
        if vf>=99999 and rangeAmp!=2: 
            vf=self.getCurrentTrip(channel,2)
        elif vf<=0 and rangeAmp!=0:
            vf=self.getCurrentTrip(channel,0)            
        return vf
        
    # Read status word
    def getStatusWord(self,channel):
        cmd = 'S%i'%(channel)
        writeStatus = self.sc.writeAndRead(cmd)
        readStat = self.sc.read()
        return readStat[3:]
        
    # Read module status
    def getModuleStatus(self,channel):
        cmd = 'T%i'%(channel)
        modStat = self.sc.writeAndRead(cmd)
        readModStat = self.sc.read()
        return int(readModStat)
        
    # Write auto start channel 1
    def setAutoStart(self,channel,autoVal=8):
        cmd = 'A%i=%i'%(channel,autoVal)
        autoStart = self.sc.writeAndRead(cmd)
        readStart = self.sc.read()
        return True
        
    # Read auto start channel
    def getAutoStart(self,channel):
        cmd = 'A%i'
        autoStart1 = self.sc.writeAndRead(cmd)
        readAuto = self.sc.read()
        return readAuto
    pass
        
