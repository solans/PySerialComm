#!/usr/bin/env python
#######################################
# Standa Linear Stage Controller GUI
#
# Florian.Haslbeck@cern.ch
# December 2019
# based on Abhishek.Sharma@cern.ch 's 
# Isel_GUI.py
#######################################



import sys
from PyQt5.QtWidgets import QWidget, QLabel, QApplication,QInputDialog,QLineEdit,QPushButton,QApplication, QMainWindow
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.Qt import Qt
from PyQt5.QtCore import QEvent
import IselLES5
import StandaAxis
import time

class Example(QMainWindow):
    
    def __init__(self):
        super(Example,self).__init__()
        self.title = "Standa Linear Stage Control"
        self.left = 10
        self.top = 10
        self.width = 572
        self.height = 430
        self.initUI()
            
    def initUI(self):
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.motorx = StandaAxis.StandaAxis('x','/dev/ttyACM0')  #may need FIX
        self.motory = StandaAxis.StandaAxis('y','/dev/ttyACM2')  #may need FIX
        self.motorz = StandaAxis.StandaAxis('z','/dev/ttyACM1')  #may need FIX
       
        #self.motorx.setVerbose(True)
        #self.motory.setVerbose(True)
        #self.motorz.setVerbose(True)

        print("Check if current position is in axes' ranges")
        if not self.motorx.checkRange(self.motorx.getPos()):
            print("x: out of range -> move home")
            self.motorx.setPos(50)
            pass
        if not self.motory.checkRange(self.motory.getPos()):
            print("y: out of range -> move home")
            self.motory.setPos(50)
            pass
        if not self.motorz.checkRange(self.motorz.getPos()):
            print("z: out of range -> move home")
            self.motorz.setPos(50)
            pass
        
        print("Current Position is:")
        self.motorx.waitForStop()
        print("%s: pos [mm] \t%.3f"%("x",self.motorx.getPos()))
        self.motory.waitForStop()     
        print("%s: pos [mm] \t%.3f"%("y",self.motory.getPos()))
        self.motorz.waitForStop()      
        print("%s: pos [mm] \t%.3f"%("z",self.motorz.getPos()))

        print("\nready to go\n")
        #label = QLabel(self)
        #pixmap = QPixmap('GUI.png')
        #label.setPixmap(pixmap)
        
        label = QLabel(self)
        #label.move(10,10)
        label.resize(572,430)
        pixmap = QPixmap('GUI2.png')
        label.setPixmap(pixmap)
        #self.resize(pixmap.width(), pixmap.height())

        #lay.addWidget(label)

        # Optional, resize window to image size
        #self.resize(pixmap.width(),pixmap.height())
       
        #'''
        self.currPosTitle = QLabel('Current\nPosition', self)
        self.currPosTitle.move(225, 10)        

        self.xPosTitle = QLabel('X position', self)
        self.xPosTitle.move(25, 45)

        self.yPosTitle = QLabel('Y position', self)
        self.yPosTitle.move(25, 75)

        self.zPosTitle = QLabel('Z position', self)
        self.zPosTitle.move(25, 105)

        self.xPos = QLineEdit(self)
        self.xPos.move(120, 45)

        self.yPos = QLineEdit(self)
        self.yPos.move(120, 75)
        
        self.zPos = QLineEdit(self)
        self.zPos.move(120, 105)



        self.upB = QPushButton('Up', self)
        self.upB.move(245,205)
        #while button.pressed.connect()
        self.upB.clicked.connect(self.upb)

        self.downB = QPushButton('Down', self)
        self.downB.move(245,245)        
        self.downB.clicked.connect(self.downb)

        self.rightB = QPushButton('Right', self)
        #self.rightB.resize(160,30)
        self.rightB.move(130,225)        
        self.rightB.clicked.connect(self.rightb)

        self.leftB = QPushButton('Left', self)
        #self.leftB.resize(160,30)
        self.leftB.move(10,225)        
        self.leftB.clicked.connect(self.leftb)


        self.forwardsB = QPushButton('Forward', self)
        self.forwardsB.move(70,190)        
        self.forwardsB.clicked.connect(self.forwardsb)

        self.backwardsB = QPushButton('Backwards', self)
        self.backwardsB.move(70,260)        
        self.backwardsB.clicked.connect(self.backwardsb)

        '''
        self.confB = QPushButton('Configure', self)
        self.confB.move(15,10)        
        self.confB.clicked.connect(self.conf)
        '''

        self.readB = QPushButton('Read', self)
        self.readB.move(120,10)        
        self.readB.clicked.connect(self.read)

        self.moveB = QPushButton('Move', self)
        self.moveB.move(120,140)        
        self.moveB.clicked.connect(self.move)

        self.homeB = QPushButton('Home', self)
        self.homeB.move(20,140)        
        self.homeB.clicked.connect(self.home)

        self.quitB = QPushButton('Quit', self)
        self.quitB.move(400,10)        
        self.quitB.clicked.connect(self.close)

        self.xPosCurr = QLabel('        ', self)
        self.xPosCurr.move(235, 45)

        self.yPosCurr = QLabel('        ', self)
        self.yPosCurr.move(235, 75)

        self.zPosCurr = QLabel('        ', self)
        self.zPosCurr.move(235, 105)


        self.setWindowTitle(self.title)
        

        
        self.show()

        '''for i in xrange(5):
            print "The keyboard arrow buttons will be activated in %i seconds!"%(5-i)
            if i==4:
                print "The keyboard arrows are now linked to the linear stage!!!"
                pass
            time.sleep(2)
            pass
        '''
        pass

    def keyPressEvent(self, event): 
        if event.key() == Qt.Key_Up and event.isAutoRepeat()==False:
            if event.type()==QEvent.KeyPress: self.upb()
        if event.key() == Qt.Key_Down and event.isAutoRepeat()==False:
            if event.type()==QEvent.KeyPress: self.downb()
        if event.key() == Qt.Key_Left and event.isAutoRepeat()==False:
            if event.type()==QEvent.KeyPress: self.leftb()
        if event.key() == Qt.Key_Right and event.isAutoRepeat()==False:
            if event.type()==QEvent.KeyPress: self.rightb()
        if event.key() == Qt.Key_Space and event.isAutoRepeat()==False:
            if event.type()==QEvent.KeyPress: self.home()

    def tmp(self):
        print "ok"

    def upb(self):
        print("\nMoving UP by 1mm")
        self.motorz.waitForStop()
        self.motorz.moveRel(-1)
        self.motorz.waitForStop()
        print("%s: pos [mm] \t %.3f"%(self.motorz.axis,self.motorz.getPos()))
        pass

    def downb(self):
        print("Moving DOWN by 1mm")
        self.motorz.waitForStop()
        self.motorz.moveRel(1)
        self.motorz.waitForStop()
        print("%s: pos [mm] \t %.3f"%(self.motorz.axis,self.motorz.getPos()))
        pass

    def rightb(self):
        print("Moving RIGHT on y axis 1mm")
        self.motory.waitForStop()
        self.motory.moveRel(1)
        self.motory.waitForStop()
        print("%s: pos [mm] \t %.3f"%(self.motory.axis,self.motory.getPos()))
        pass

    def leftb(self):
        print("Moving LEFT on y axis 1mm")
        self.motory.waitForStop()
        self.motory.moveRel(-1)
        self.motory.waitForStop()
        print("%s: pos [mm] \t %.3f"%(self.motory.axis,self.motory.getPos()))
        pass

    def rightb(self):
        print("Moving RIGHT on y axis 1mm")
        self.motory.waitForStop()
        self.motory.moveRel(1)
        self.motory.waitForStop()
        print("%s: pos [mm] \t %.3f"%(self.motory.axis,self.motory.getPos()))
        pass

    def forwardsb(self):
        print("Moving FORWARDS on x axis 1mm")
        self.motorx.waitForStop()
        self.motorx.moveRel(-1)
        self.motorx.waitForStop()
        print("%s: pos [mm] \t %.3f"%(self.motorx.axis,self.motorx.getPos()))
        pass

    def backwardsb(self):
        print("Moving BACKWARDS on x axis 1mm")
        self.motorx.waitForStop()
        self.motorx.moveRel(1)
        self.motorx.waitForStop()
        print("%s: pos [mm] \t %.3f"%(self.motorx.axis,self.motorx.getPos()))
        pass   



    def home(self):
        #if raw_input("Are you SURE you want to return to cold box to original alignment position in X AND Z? if yes, type YES")=="YES":
            #if raw_input("Confirm with yes")=="yes":
        #self.motorx.setSpeed(5)
        #self.motorz.setSpeed(5)
        print("Move x, y and z to 0")
        self.motorx.goHome()
        self.motory.goHome()
        self.motorz.goHome()
        pass
        #pass
        #pass


    '''
    def conf(self):
        self.motorx.configureController(1)
        self.motorx.setSpeed(20)
        self.motorz.configureController(1)
        self.motorz.setSpeed(20)
        self.statusBar().showMessage("Configuring Stages.")
        pass
    '''
    def read(self):
        x=self.motorx.getPos()
        y=self.motory.getPos()
        z=self.motorz.getPos()
        self.xPosCurr.setText("%s mm"%x)
        self.xPos.setText("%s"%x)
        self.yPosCurr.setText("%s mm"%y)
        self.yPos.setText("%s"%y)
        self.zPosCurr.setText("%s mm"%z)
        self.zPos.setText("%s"%z)
        pass

    def move(self):
        self.motorx.setPos(float(self.xPos.text()))
        self.motory.setPos(float(self.yPos.text()))      
        self.motorz.setPos(float(self.zPos.text()))      
        self.read()
        pass
    '''
    def invert(self):
        self.motorx.invertAxis(True)
        self.motorz.invertAxis(True)
        print "axes inverted."
        pass
       
    '''

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
