#!/usr/bin/env python
#######################################
#
# PSU Control GUI
#
# QT based GUI to control power supplies
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# January 2020
#
########################################


import os
import sys
import time
import signal
import PSUController
import PSURemoteController
import PSUConfig


if __name__=="__main__":


    import argparse
    parser=argparse.ArgumentParser("")
    parser.add_argument("-f","--config",help="Configuration file")
    parser.add_argument("-H","--host",help="remote host")
    parser.add_argument("-p","--port",help="remote port",default=9998,type=int)
    parser.add_argument("-v","--verbose",help="enable verbose mode",action="store_true")
    parser.add_argument("-w","--write",help="write values",action="store_true")
    parser.add_argument("-t","--turnon",help="power on all",action="store_true")
    parser.add_argument("-o","--turnoff",help="power off all",action="store_true")
    #parser.add_argument("-r","--read",help="read values",action="store_true")
    args=parser.parse_args()


    if args.host:
        controller=PSURemoteController.PSURemoteController(args.host,args.port)
        controller.setVerbose(args.verbose)
        controller.open()
        psus=controller.getPsus()
        pass
    else:
        config=PSUConfig.PSUConfig(args.config)
        psus=config.getPsus()
        controller=PSUController.PSUController(psus)
        pass




    for psu in psus:
        print("%s:"%psu,
              "{0}/ {1}/ {2} V".format(controller.getVoltage(psu),controller.getSetVoltage(psu),psus[psu]["Vmax"]), 
              "{0}/ {1}/ {2} I".format(controller.getCurrent(psu),controller.getCurrentLimit(psu),psus[psu]["Imax"]), 
              "(Measured, On PSU, On Run Card)",
              "Enabled:",controller.isEnabled(psu))




        if args.write:
            controller.rampVoltage(psu,float(psus[psu]["Vmax"]))
            controller.setCurrentLimit(psu,float(psus[psu]["Imax"]))


            print("Updated power %s:"%psu,
              "{0}/ {1}/ {2} V".format(controller.getVoltage(psu),controller.getSetVoltage(psu),psus[psu]["Vmax"]), 
              "{0}/ {1}/ {2} I".format(controller.getCurrent(psu),controller.getCurrentLimit(psu),psus[psu]["Imax"]), 
              "(Measured, On PSU, On Run Card)",
              "Enabled:",controller.isEnabled(psu))


        
        if args.turnon or args.turnoff:

            NewStatus = False
            if args.turnon:
                NewStatus = True


            controller.setOutput(psu,NewStatus)
            time.sleep(1) # Ver need this otherwise getVoltage doesn't update fast enough
            print("Updated Status %s:"%psu,
              "{0}/ {1}/ {2} V".format(controller.getVoltage(psu),controller.getSetVoltage(psu),psus[psu]["Vmax"]), 
              "{0}/ {1}/ {2} I".format(controller.getCurrent(psu),controller.getCurrentLimit(psu),psus[psu]["Imax"]), 
              "(Measured, On PSU, On Run Card)",
              "Enabled:",controller.isEnabled(psu))


            

        print()



