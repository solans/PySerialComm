#!/usr/bin/env python
import os
import time
import ROOT
import array
import datetime
import Keithley
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("min_v",help="min voltage (V)",type=float)
parser.add_argument("max_v",help="max voltage (V)",type=float)
parser.add_argument("output",help="output file")
parser.add_argument("-c","--max_c",help="max current (uA)",type=float,default=1)
parser.add_argument("-v","--v_steps",help="num steps",type=int,default=20)
parser.add_argument("-p","--port",default="/dev/ttyUSB1")
parser.add_argument("-n","--ndaqs",type=int,default=10)
parser.add_argument("-d","--debug",action='store_true')
parser.add_argument("-b","--batch",action='store_true')
args=parser.parse_args()

print "IV curve with Keithley"
print "minV (V) : %.2f" % args.min_v
print "maxV (V) : %.2f" % args.max_v
print "stepsV   : %i" % args.v_steps
print "ndaqs    : %i" % args.ndaqs
print "maxC (uA): %i" % args.max_c
print "port     : %s" % args.port

k = Keithley.Keithley(args.port)
k.setCurrentLimit(args.max_c/1E6)
k.setVoltage(args.min_v)
k.enableOutput(True)

print "Open ROOT file allocate Tree"
fw = ROOT.TFile(args.output,"RECREATE")                    
ntuple       = ROOT.TTree("IV","")
timestamp    = array.array('L',(0,))      #seconds since epoch
voltage      = array.array('f',(0,))      #voltage
current      = array.array('f',(0,))      #current
ntuple.Branch("timestamp", timestamp, "timestamp/i")
ntuple.Branch("voltage",   voltage,   "voltage/F")
ntuple.Branch("current",   current,   "current/F")

hIvsV = ROOT.TProfile("hIvsV",";Bias (V);Current (uA)",args.v_steps,args.min_v,
                      args.max_v+(args.max_v-args.min_v)/(args.v_steps-1))

print "Start scan"
for i in xrange(args.v_steps):
    volt = args.min_v + (args.max_v-args.min_v)/(args.v_steps-1)*i
    print "Set voltage (V) : %.2f" % volt
    k.setVoltage(volt)
    time.sleep(1)
    for j in xrange(args.ndaqs):
        curr = float(k.getCurrent())
        hIvsV.Fill(volt,curr)
        voltage[0]=volt
        current[0]=curr*1E6
        timestamp[0]=int(time.time())
        ntuple.Fill()
        print "Get current (uA): %.3f" % (curr*1E6)
        pass
    pass

print "Close file"
ntuple.Write()
hIvsV.Write()
fw.Close()

print "Draw results"
cIvsV = ROOT.TCanvas("cIvsV","I vs V",800,600)
hIvsV.Draw()
cIvsV.Update()

if not args.batch: raw_input()
