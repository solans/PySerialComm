#!/usr/bin/env python
##################################################################
# This is a test script (binary) for the KDC101
# florian.haslbeck@cern.ch
# May 2022
##################################################################

import os
import sys
import argparse
import ThorlabsKDC101
from time import sleep

parser=argparse.ArgumentParser('Test KDC stage')
parser.add_argument('-p','--port',help="USB port (/dev/ttyACM1)", required=True)
parser.add_argument('-m','--move',type=float,help="move to position")
parser.add_argument('--home',help="go home",action="store_true")
parser.add_argument('-v','--verbose',help="verbose",action="store_true")
args=parser.parse_args()

print("Create new port:%s" % (args.port))
ax=ThorlabsKDC101.ThorlabsKDC101(args.port)

#msg = ax.get_status()
#print('Status: ' , msg)


print('get old positon')
pos_old_deg = ax.getPos()
print('old pos: ', pos_old_deg, 'deg')
pos_old_steps = ax.getPos(inSteps=True)
print('old pos: ', pos_old_steps, 'steps')

# print('now move by 10 steps')
#deg = -10
#ax.moveRel(deg, inSteps=False)



# print('now move to 100 deg')
# ax.moveAbs(100)
# print('wait 2 seconds')
# sleep(5)
# ax.stop()
# print('stopped')
# ax.waitForStop()
# print('done')

# del ax # somehow a new insance is needed?!
# ax=ThorlabsKDC101.ThorlabsKDC101(args.port)

# msg = ax.get_status()
# print('new status ', msg)
# print('now get the new position')
# pos_new = ax.getPos()
# print('new pos: ', pos_new)

# print('diff (%.f), %.f'%(deg, pos_new-pos_old))

# #ax.moveRel(50000)
# print('so far so good')

# '''

# if args.move:
#     print("Move to position: %i" % args.move)
#     ax.setPos(args.move)
#     ax.waitForStop()
#     ax.getPos()
# '''
# print("Have a nice day")




