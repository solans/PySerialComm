#!/usr/bin/env python

import Keithley

k = Keithley.Keithley("/dev/cu.usbserial-FTZ5U8CXB")
k.userCmd("*RST")
k.userCmd(":TRAC:CLE")
k.userCmd(":TRAC:POIN 10") 
k.userCmd(":STAT:MEAS:ENAB 512") #generate an SRQ upon buffer full (GPIB only!)
k.userCmd("*SRE 1")
k.userCmd(":TRIG:COUN 10") #trigger count
k.userCmd(":SYST:AZER:STAT OFF") #Auto Zero off
k.userCmd(":SOUR:FUNC CURR") #source current
k.userCmd(":SENS:FUNC:CONC OFF") #concurrent readings off
k.userCmd(":SENS:FUNC 'VOLT'") #measure voltage 
k.userCmd(":SENS:VOLT:NPLC 0.1")
k.userCmd(":SENS:VOLT:RANG 20")
k.userCmd(":SENS:VOLT:PROT:LEV 1.2") #voltage compliance
k.userCmd(":FORM:ELEM VOLT")
k.userCmd(":SOUR:CURR 0.01")
k.userCmd(":TRIG:DEL 0")
k.userCmd(":SOUR:DEL 0")
k.userCmd(":TRAC:FEED:CONT NEXt")
k.userCmd(":SOUR:CLE:AUTO ON")
k.userCmd(":INIT")
#k.userCmd(":TRAC:DATA?")
k.userCmd("*RST")
#k.userCmd("*CLS")
#k.userCmd("*SRE 0")
#k.userCmd(":STAT:MEAS:ENAB 0")




