#!/usr/bin/env python
#######################################
#
# PSU Control GUI
#
# QT based GUI to control power supplies
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# January 2020
#
########################################


import os
import sys
import time
import signal
import PSUController
import PSURemoteController
import PSUConfig

from PyQt5 import QtGui,QtCore,QtWidgets
    
class PSUControlGUI(QtWidgets.QMainWindow):
    
    def __init__(self, psus, controller):
        super(PSUControlGUI, self).__init__()
        QtWidgets.QApplication.setStyle("Plastique")
        self.setWindowTitle("PSU Control GUI")
        self.setGeometry(0,0,700,250)
        self.controller=controller
        self.psus=psus
        self.create()
        
    def create(self):
        
        wid = QtWidgets.QWidget(self)
        self.wid=wid
        self.setCentralWidget(wid)

        self.lName = QtWidgets.QLabel("Output",wid)
        self.lOutp = QtWidgets.QLabel("Output",wid)
        self.lVset = QtWidgets.QLabel("Vset [V]",wid)
        self.lIlim = QtWidgets.QLabel("Ilim [A]",wid)
        self.lVout = QtWidgets.QLabel("Vout [V]",wid)
        self.lIout = QtWidgets.QLabel("Iout [A]",wid)

        self.lName.move(20+80*0,15)
        self.lOutp.move(20+80*1,15)
        self.lVset.move(20+80*2,15)
        self.lIlim.move(20+80*3,15)
        self.lVout.move(20+80*4,15)
        self.lIout.move(20+80*5,15)
        
        self.tName={}
        self.tOutp={}
        self.tVset={}
        self.tIlim={}
        self.tVmax={}
        self.tVout={}
        self.tIout={}
        self.bRead={}
        self.bWrite={}
        
        iline=0
        for psu in self.psus:
            print ("Add PSU: %s" % psu)
            
            self.tName[psu] = QtWidgets.QLabel("%s"%psu,wid)
            self.tOutp[psu] = QtWidgets.QCheckBox(wid)
            self.tVset[psu] = QtWidgets.QLineEdit(wid)
            self.tIlim[psu] = QtWidgets.QLineEdit(wid)
            self.tVout[psu] = QtWidgets.QLineEdit(wid)
            self.tIout[psu] = QtWidgets.QLineEdit(wid)
            self.bRead[psu] = QtWidgets.QPushButton("read",wid)
            self.bWrite[psu] = QtWidgets.QPushButton("write",wid)

            self.tName[psu].move(20+80*0,40+30*iline)
            self.tOutp[psu].move(20+80*1,40+30*iline)
            self.tVset[psu].move(20+80*2,40+30*iline)
            self.tIlim[psu].move(20+80*3,40+30*iline)
            self.tVout[psu].move(20+80*4,40+30*iline)
            self.tIout[psu].move(20+80*5,40+30*iline)
            self.bRead[psu].move(20+80*6,40+30*iline)
            self.bWrite[psu].move(20+80*7,40+30*iline)
            
            self.tVset[psu].resize(50,20)
            self.tIlim[psu].resize(60,20)
            self.tVout[psu].resize(50,20)
            self.tIout[psu].resize(60,20)
            
            self.tOutp[psu].setChecked(False)
            self.tOutp[psu].stateChanged.connect(lambda state, x=psu: self.enable(x))
            self.bRead[psu].clicked.connect(lambda state, x=psu : self.read(x))
            self.bWrite[psu].clicked.connect(lambda state, x=psu : self.write(x))
            
            iline+=1
            pass
        
        self.statusBar().showMessage("Loaded")
        self.show()
        pass
    
    def enable(self,psu):
        print ("PSU %s output %r " % (psu,self.tOutp[psu].isChecked()))
        self.controller.setOutput(psu,self.tOutp[psu].isChecked())
        pass

    def read(self,psu):
        self.statusBar().showMessage("Reading values...")
        QtWidgets.QApplication.processEvents()
        iena = self.controller.isEnabled(psu)
        self.tOutp[psu].blockSignals(True)
        self.tOutp[psu].setChecked(iena)
        self.tOutp[psu].blockSignals(False)
        vset = self.controller.getSetVoltage(psu)
        self.tVset[psu].setText(str(vset))
        clim = self.controller.getCurrentLimit(psu)            
        self.tIlim[psu].setText(str(clim))
        vout = self.controller.getVoltage(psu)
        self.tVout[psu].setText(str(vout))
        cout = self.controller.getCurrent(psu)
        self.tIout[psu].setText(str(cout))
        self.statusBar().showMessage("Last updated: "+time.strftime("%Y-%m-%d %H:%M:%S"))
        QtWidgets.QApplication.processEvents()
        pass

    
    def write(self,psu):
        self.statusBar().showMessage("Writing values...")
        QtWidgets.QApplication.processEvents()
        try:
            val = "%.3f"%float(self.tVset[psu].text())
            self.tVset[psu].setStyleSheet("")
            self.controller.rampVoltage(psu,float(val))
        except ValueError:
            self.tVset[psu].setStyleSheet("background: red;")
            pass
        try:
            val = "%.4f"%float(self.tIlim[psu].text())
            self.tIlim[psu].setStyleSheet("")
            self.controller.setCurrentLimit(psu,float(val))
        except ValueError:
            self.tIlim[psu].setStyleSheet("background: red;")
            pass
        self.statusBar().showMessage("Last updated: "+time.strftime("%Y-%m-%d %H:%M:%S"))
        QtWidgets.QApplication.processEvents()
        pass


    def close_application(self):
        choice = QtGui.QMessageBox.question(self, "Quitting...",
                                            "Are you sure you wish to quit?",
                                            QtGui.QMessageBox.Yes |
                                            QtGui.QMessageBox.No)
        if choice == QtGui.QMessageBox.Yes:
            print("Quitting...")
            sys.exit()
        else: pass
        pass


if __name__=="__main__":

    rerun = False
    req_enum="%(LCG_INST_PATH)s/LCG_94/enum34/1.1.6/%(CMTCONFIG)s/lib/python2.7/site-packages"%os.environ
    req_free="%(LCG_INST_PATH)s/LCG_94/freetype/2.6.3/%(CMTCONFIG)s/lib"%os.environ
    sys.path.insert(0,req_enum)
    sys.path.insert(0,req_free)
    if not req_enum in sys.path:
        print ("Enum not found")
        os.environ['PYTHONPATH']+=":"+req_enum
        rerun=True
        pass
    if not req_free in os.environ['LD_LIBRARY_PATH']:
        print ("Free not found")
        os.environ['LD_LIBRARY_PATH']=req_free+":"+os.environ['LD_LIBRARY_PATH']
        rerun=True
        pass
    if rerun:
        os.execve(os.path.realpath(__file__), sys.argv, os.environ)
        pass

    import argparse
    parser=argparse.ArgumentParser("")
    parser.add_argument("-f","--config",help="Configuration file")
    parser.add_argument("-H","--host",help="remote host")
    parser.add_argument("-p","--port",help="remote port",default=9998,type=int)
    parser.add_argument("-v","--verbose",help="enable verbose mode",action="store_true")
    args=parser.parse_args()

    controller=None
    psus=None
    if args.host:
        controller=PSURemoteController.PSURemoteController(args.host,args.port)
        controller.setVerbose(args.verbose)
        controller.open()
        psus=controller.getPsus()
        pass
    else:
        config=PSUConfig.PSUConfig(args.config)
        psus=config.getPsus()
        controller=PSUController.PSUController(psus)
        pass
        
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtWidgets.QApplication(sys.argv)
    gui = PSUControlGUI(psus,controller)
    def aboutToQuit():
        return gui.close_application()
    #app.aboutToQuit.connect(aboutToQuit)
    sys.exit(app.exec_())
