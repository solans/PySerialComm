#!/usr/bin/python

import os
import sys
import argparse
import SerialCom

parser=argparse.ArgumentParser()
parser.add_argument('--port',help="port",required=True)
parser.add_argument('--baudrate',help="baudrate",type=int,required=True)
parser.add_argument('-v','--verbose',help="enable verbose mode",action="store_true")
args=parser.parse_args()

sc = SerialCom.SerialCom(args.port,args.baudrate)
sc.setVerbose(args.verbose)
version=sc.writeAndRead("*IDN?")
print("Version: %s"%version)

