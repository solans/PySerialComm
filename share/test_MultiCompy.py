#!/usr/bin/env python3
# florian.haslbeck@cern.ch
# June 2023

import MultiComp
import time

import argparse
parser=argparse.ArgumentParser()
parser.add_argument("-p","--port",help='port', default = '/dev/ttyUSB0')
parser.add_argument('--sleep', help='Time between measurements in seconds', type=int, default=5)
args=parser.parse_args()

# initalise the class and give it the port
mc = MultiComp.MultiComp(args.port)



# make a reading of the weight every 1s
# the scale is flushing out continously.
# so best is probably *to be checked* 
# to read every 1s and only save now and then   

while True:
    print(mc.read_weight_grams())
    input("Press any key to continue")
    # time.sleep(args.sleep)


