#!/usr/bin/env python

import argparse
from StandaAxis import StandaAxis

parser = argparse.ArgumentParser()
parser.add_argument('-v','--verbose',action='store_true')
args=parser.parse_args()

print("Standa Axis X")
axisX=StandaAxis("/dev/serial/by-id/usb-XIMC_XIMC_Motor_Controller_000046C6-if00")
axisX.setVerbose(args.verbose)
axisX.printStatus()
axisX.printFirmwareVersion()

print("Standa Axix Z")
axisZ=StandaAxis("/dev/serial/by-id/usb-XIMC_XIMC_Motor_Controller_00004AA9-if00")
axisZ.setVerbose(args.verbose)
axisZ.printStatus()
axisZ.printFirmwareVersion()
