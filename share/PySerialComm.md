# PySerialComm {#PackagePySerialComm}

PySerialComm provides helper classes to control power supplies and other devices through the serial port in pure Python.
This requires pyserial to be installed.

## Python modules

- SerialComm: class that implements the serial commnunication (read, write, open, close)
- PSUConfig: generic description of a power supply configuration
- PSUController: tool to control a power supply using PSUConfig

### Implemented power supplies

- TTi: TTi PL303QMD-P
- agilent_e3631a: Agilent E3631A
- agilent_e3646a: Agilent E3646A
- TTi_QL355TP: TTi QL355T-P
- HAMEG4040: HAMEG 4040
- Iseg: ISEG SHQ-224M
- Keithley: Keithley 2400
- TenmaPSU: Model 72-2795

### Implemented linear stages

- IselLES5: ISEL LES linear stage
- StandaAxis: Standa stages using 8SMC4 protocol
- ThorlabsKDC101: Thorlabs KDC 101 rotational stage
- GroningenLinearStage: Linear stage used at Groningen Cyclotron
- LinearStage: Berger Larhr ICIA IDS-MIO

### Other devices

- cts_t65: CTS T-65 climate chamber 
- MultiComp: MultiComp digital scale 800 g x 0.01

## Scripts

- KDC101AxisCtrl.py: Command line interface for the ThorlabsKDC101
- MoveIselLinearStage.py: Command line interface for IselLES5
- PSUControlGUI.py: Graphical intreface for PSUController
- StandaAxisInfo.py: Command line interface for StandaAxis
- StandaXAxisCtrl.py: Control the StandaAxis in X in the MALTA testbeam
- StandaZAxisCtrl.py: Control the StandaAxis in Z in the MALTA testbeam
