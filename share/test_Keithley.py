#!/usr/bin/env python
import Keithley
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-p","--port",required=True)
args=parser.parse_args()

keith = Keithley.Keithley(args.port)
print ("GetModel: %s" % keith.getModel())
keith.close()

