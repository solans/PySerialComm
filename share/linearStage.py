#!/usr/bin/env python
import os
import sys
import LinearStage

stage = LinearStage.LinearStage('/dev/tty.usbserial-CTU8NP3O')
stage.setVerbose(True)

raw_input("press any key to get fault")
stage.resetFault()

raw_input("press any key to get action status")
stage.getActionStatus()

raw_input("press any key to get stop fault")
stage.getStopFault()

raw_input("press any key to get brake")
stage.getBrake()

raw_input("press any key to get monitoring")
stage.getMonitoring()

raw_input("press any key to set home position")
stage.setHomePosition(1)

raw_input("press any key to power up")
stage.setPower(True)

raw_input("press any key to power down")
stage.setPower(False)
