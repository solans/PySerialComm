#!/usr/bin/env python
import os
import time
import agilent_e3646a
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-p","--port",default="/dev/ttyUSB1")
parser.add_argument("-out","--out",help="output supply: 1 or 2",type=int,required=True)
parser.add_argument("-volt","--volt",help="voltage (V)",type=float,required=True)
parser.add_argument("-vlim","--vlim",help="voltage limit (V)",type=float,default=1)
parser.add_argument("-clim","--clim",help="current limit (A)",type=float,default=1)
parser.add_argument("-w","--wait",help="wait between steps (s)",type=float,default=0.01)
parser.add_argument("-s","--steps",help="number of steps. Default 1",type=int,default=1)
parser.add_argument("-rst","--reset",help="reset power supply",type=int,default=0)
args=parser.parse_args()

print "\nTest agilent"
print "Port         : %s"     % args.port
print "Output       : %i"     % args.out
print "V_set     (V): %.2f"   % args.volt
print "V_lim     (V): %.2f"   % args.vlim
print "C_lim     (A): %.2f"   % args.clim
print "Steps        : %i"     % args.steps
print "Wait      (s): %.3f\n"   % args.wait

agilent = agilent_e3646a.agilent_e3646a(args.port)
agilent.enableRemote()
if args.reset == 1:
    agilent.reset()
    pass
agilent.enableOutput(True)
agilent.setOutput(args.out)
agilent.setCurrentLimit(args.clim)
#agilent.setVoltage(args.volt)
agilent.rampVoltage(0,args.volt,args.steps,args.wait)
print "Voltage (V): %.5f" % float(agilent.getVoltage())
print "Current (A): %.5f" % float(agilent.getCurrent())
if (float(agilent.getVoltage())>args.vlim) or (float(agilent.getCurrent())>args.clim):
    agilent.enableOutput(False)
    print "Safety switch off"
    pass
#print "%s" % agilent.systemError()
#agilent.enableOutput(False)
agilent.close()
