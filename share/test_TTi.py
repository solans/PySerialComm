#!/usr/bin/env python
import argparse
import TTi

parser=argparse.ArgumentParser()
parser.add_argument("-p","--port",required=True)
args=parser.parse_args()

tti = TTi.TTi(args.port)
print ("GetModel: %s" % tti.getModel())
tti.close()
